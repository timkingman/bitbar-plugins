#!/usr/bin/osascript

# Metadata allows your plugin to show up in the app, and website.
#
#  <xbar.title>BBEdit Open Documents</xbar.title>
#  <xbar.version>v1.0</xbar.version>
#  <xbar.author>Tim Kingman</xbar.author>
#  <xbar.author.github>timkingman</xbar.author.github>
#  <xbar.desc>Counts open BBEdit documents (unsaved/total).</xbar.desc>
#  <xbar.image></xbar.image>
#  <xbar.dependencies></xbar.dependencies>
#  <xbar.abouturl></xbar.abouturl>

tell application "BBEdit"
	set allDocuments to documents
	set modifiedDocuments to documents whose modified is true
	return "" & length of modifiedDocuments & "/" & length of allDocuments
end tell

